defmodule Main do
  def fetch_data(receiver) do fn ->
    words = ['cat','bird', 'fish', 'dog', 'mouse']
    :timer.sleep(1000)
    send receiver, {:animal, Enum.random(words)}
  end end

  def fetch_sentence([a1,a2,a3]) do
      IO.puts ["The ", a1, " and ", a2,
               " catches the ", a3, " by the tail"]
  end
  def fetch_sentence(animalList) do
    receive do
      {:animal, name} ->
        fetch_sentence([name|animalList])
    end
  end
end

current_process = self()

spawn_link(Main.fetch_data(current_process))
spawn_link(Main.fetch_data(current_process))
spawn_link(Main.fetch_data(current_process))

Main.fetch_sentence []
