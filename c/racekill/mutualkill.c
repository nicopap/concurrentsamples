#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <assert.h>

int main(int argc, char *argv[])
{
	int pid = fork();
	assert(pid >= 0);
	if (pid == 0) {
		kill(getppid(), SIGINT);
		printf("child");
	} else {
		for (volatile int i = 0; i < 18000; ++i);
		kill(pid, SIGINT);
		printf("parent");
	}
	return 0;
}
