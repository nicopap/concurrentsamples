// Loosly based on the following:
// http://timmurphy.org/2010/05/04/pthreads-in-c-a-minimal-working-example/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct semint {
	int value;
};

pthread_mutex_t semint_mutex;
pthread_cond_t semint_cv;
void write_semint(struct semint* semintvar, int value) {
	pthread_mutex_lock(&semint_mutex);
	semintvar->value = value;
	pthread_cond_signal(&semint_cv);
	pthread_mutex_unlock(&semint_mutex);
}

int read_semint(struct semint* semintvar) {
	pthread_mutex_lock(&semint_mutex);
	pthread_cond_wait(&semint_cv, &semint_mutex);
	int value = semintvar->value;
	pthread_mutex_unlock(&semint_mutex);

	return value;
}

pthread_mutex_t stdout_mutex;

void create_thread(void *toRun(void*), struct semint* arg) {
	pthread_t other_thread;

	if(pthread_create(&other_thread, NULL, toRun, (void*) arg)) { //producer
		fprintf(stderr, "Error creating thread\n");
		exit(1);
	}
}

void *collect_guess(void *guess_ptr) {
	struct semint* semguess = (struct semint*) guess_ptr;
	int guess;

	while (1) {
		pthread_mutex_lock(&stdout_mutex);
		printf("\nguess a number: ");
		if (scanf("%d", &guess) == 1) {
			write_semint(semguess, guess);
		}
		pthread_mutex_unlock(&stdout_mutex);
	}
}

void *process_guess(void *guess_ptr) {
	struct semint* semguess = (struct semint*) guess_ptr;
	int guess;
	int answer = 1 + rand() % 10;

	while (1) {
		guess = read_semint(semguess);
		pthread_mutex_lock(&stdout_mutex);
		if (guess == answer) {
			printf("%d is the correct answer\n", guess);
			break;
		} else {
			printf("%d is wrong!\n", guess);
		}
		pthread_mutex_unlock(&stdout_mutex);
	}
}

int main() {
	struct semint guess = {0};

	srand(time(NULL));
	create_thread(collect_guess, &guess); //producer
	process_guess(&guess); //consumer
	printf("\nCongratulations!\n");
	return 0;
}
