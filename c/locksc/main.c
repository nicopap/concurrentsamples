#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

pthread_mutex_t mutex;
pthread_cond_t cv;
void lock_write(long* toWrite, long value) {
	pthread_mutex_lock(&mutex);
	*toWrite = value;
	pthread_mutex_unlock(&mutex);
}

long lock_read(long* toRead) {
	pthread_mutex_lock(&mutex);
	long value = *toRead;
	pthread_mutex_unlock(&mutex);
	return value;
}

void thread(void *toRun(void*), long* arg) {
	pthread_t other_thread;

	if(pthread_create(&other_thread, NULL, toRun, (void*) arg)) { //producer
		fprintf(stderr, "Error creating thread\n");
		exit(1);
	}
}

void *collect_sum(void *sum_ptr) {
	long* sum = (long*) sum_ptr;
	int i = 1;
	while (1) {
		printf("%d: %d\n",i, lock_read(sum));
		sleep(1);
		i+=1;
	}
}

void *incr(void *sum_ptr) {
	long* sum = (long*) sum_ptr;
	while (1) {
		long tmp = lock_read(sum);
		lock_write(sum, tmp + 1);
		sleep(1);
	}
}

int main() {
	long sum = 0;
	for (int i = 0; i < 10; i++) {
		thread(incr, &sum);
	}
	thread(collect_sum,&sum);
	sleep(10000);
	return 0;
}
