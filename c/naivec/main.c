#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

void thread(void *toRun(void*), long* arg) {
	pthread_t other_thread;

	if(pthread_create(&other_thread, NULL, toRun, (void*) arg)) { //producer
		fprintf(stderr, "Error creating thread\n");
		exit(1);
	}
}

void *collect_sum(void *sum_ptr) {
	long* sum = (long*) sum_ptr;
	int i = 1;
	while (1) {
		printf("%d: %d\n",i, *sum);
		sleep(1);
		i+=1;
	}
}

void *incr(void *sum_ptr) {
	long* sum = (long*) sum_ptr;
	while (1) {
		long tmp = *sum;
		*sum = tmp + 1;
		sleep(1);
	}
}

int main() {
	long sum = 0;
	thread(incr, &sum); thread(incr, &sum);
	thread(incr, &sum); thread(incr, &sum);
	thread(incr, &sum); thread(incr, &sum);
	thread(incr, &sum); thread(incr, &sum);
	thread(incr, &sum); thread(incr, &sum);
	thread(collect_sum,&sum);
	sleep(10000);
	return 0;
}
