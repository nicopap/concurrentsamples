#include <pthread.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

void thread(void *toRun(void*), long* arg) {
	pthread_t other_thread;

	if(pthread_create(&other_thread, NULL, toRun, (void*) arg)) { //producer
		fprintf(stderr, "Error creating thread\n");
		exit(1);
	}
}

pthread_mutex_t mutex;
pthread_cond_t cv;
void *collect_sum(void *sum_ptr) {
	long* sum = (long*) sum_ptr;
	int i = 1;
	while (1) {
		pthread_mutex_lock(&mutex);
		pthread_cond_wait(&cv, &mutex);
		printf("%d: %d\n",i, *sum);
		pthread_mutex_unlock(&mutex);
		i+=1;
	}
}

void *incr(void *sum_ptr) {
	long* sum = (long*) sum_ptr;
	while (1) {
		pthread_mutex_lock(&mutex);
		long tmp = *sum;
		*sum = tmp + 1;
		if (*sum % 10 == 0) pthread_cond_signal(&cv);
		pthread_mutex_unlock(&mutex);
		sleep(1);
	}
}

int main() {
	long sum = 0;
	for (int i = 0; i < 10; i++) {
		thread(incr, &sum);
	}
	thread(collect_sum,&sum);
	sleep(10000);
	return 0;
}
