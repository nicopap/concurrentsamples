import concurrent.futures as futures
import random
import time

words = ['cat','bird', 'fish', 'dog', 'mouse']
def fetch_data():
    time.sleep(random.randrange(3,6))
    return random.choice(words)


executor = futures.ProcessPoolExecutor()
animal1 = executor.submit(fetch_data)
animal2 = executor.submit(fetch_data)
animal3 = executor.submit(fetch_data)

print('The %s and %s catches the %s by the tail' %
      (animal1.result(), animal2.result(), animal3.result()))
