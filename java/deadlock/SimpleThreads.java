public class SimpleThreads {
private static class Incr implements Runnable {
	private Sum sum;
	public Incr(Sum sum) { this.sum = sum; }
	public void run() {
		while (true) {
			sum.incr();
			try { Thread.sleep(1000);
			} catch (InterruptedException e) {}
		}
	}
}
private static class CollectSum implements Runnable {
	private Sum sum;
	public CollectSum(Sum sum) { this.sum = sum; }
	public void run() {
		int i = 1;
		while (true) {
			sum.collectSum(i);
			i += 1;
		}
	}
}

private static class Sum {
	private int sum=0;
	private boolean multiple=false;

	public synchronized void incr() {
		int tmp = this.sum;
		this.sum = tmp + 1;
		if (this.sum % 10 == 0) {
			this.multiple = true;
			notifyAll();
		}
	}
	public synchronized void collectSum(int i) {
		while (!this.multiple) {
			try {wait();
			} catch (InterruptedException e) {}
		}
		this.multiple = false;
		System.out.format("%d: %d\n", i, this.sum);
	}
}

public static void main(String args[]) throws InterruptedException {
	Sum sum = new Sum();
	for (int i = 0; i < 10; i++) {
		(new Thread(new Incr(sum))).start();
	}
	(new Thread(new CollectSum(sum))).start();
	Thread.sleep(10000000);
}

}
