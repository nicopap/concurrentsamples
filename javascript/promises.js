'use strict';

function wait(delay) {
    return new Promise(resolve =>
		setTimeout(resolve, delay * 1000)
	);
}

function fetch_data() {
	let words = ['cat','bird', 'fish', 'dog', 'mouse'];
	let sleepTime = 3 * Math.random() + 3;

	return wait(sleepTime).then( () => {
		let randIndex = Math.ceil(Math.random() * words.length);
		return (words[randIndex]);
	});
}

function fetch_sentence() {
	return Promise.all([
		"suricate",
		fetch_data(),
		fetch_data()
	]).then( ([ animal1, animal2, animal3 ]) => {
		return `The ${animal1} and ${animal2} catches the ${animal3} by the tail`;
	});
}

fetch_sentence().then( (sentence) => {
	console.log(sentence);
});
