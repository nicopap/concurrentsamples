-- loosly based on https://www.lua.org/pil/9.2.html
collect_guess = coroutine.create(function() -- producer
	while true do
		io.write("\nguess a number: ")
		local guess = tonumber(io.read())
		coroutine.yield(guess)
	end
end)

function process_guess(collector)
	local answer = math.random(1,10)
	while true do
		local _, guess_val = coroutine.resume(collector)
		if guess_val == answer then
			io.write(guess_val, " is the correct answer\n")
			break
		else
			io.write(guess_val, " is wrong!\n")
		end
	end
end

-- main:
math.randomseed(os.time())
process_guess(collect_guess)
io.write("\nCongratulation!\n")
