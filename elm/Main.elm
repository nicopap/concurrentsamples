module Main exposing (main)

import Array
import Task exposing (andThen, succeed, Task)
import Time
import Debug
import Random as Rand
import Process exposing (sleep)
import Html exposing (button, div, text, br, span, Html)
import Html.Events exposing (onClick)

main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = \_ -> Sub.none
        }

type alias Model =
    { nameList : List String
    , nameCounter : Int
    }

type Msg
    = GetRandom
    | FetchRandom (Int, Float)
    | FetchData String
    | Reset

init : (Model, Cmd Msg)
init =  Model [] 0 ! []

getElem : Int -> List String -> String
getElem idx words =
    let arrayList = Array.fromList words
        arrayLen = Array.length arrayList
    in case Array.get (idx % arrayLen) arrayList of
            Just word -> word
            Nothing -> ""

fetchData : Int -> Float -> Int -> Task x String
fetchData returnIndex randomWait counter =
    let
        words = ["cat","bird", "fish", "dog", "mouse", "alpaca"]
        word =  getElem returnIndex words ++ toString counter
    in
        sleep (Time.second * randomWait)
            |> andThen (\() -> succeed word)


update : Msg -> Model -> (Model, Cmd Msg)
update msg ({ nameList, nameCounter } as model) =
    case msg of
        GetRandom ->
            ( model
            , Rand.generate FetchRandom
                (Rand.pair (Rand.int 0 100) (Rand.float 0.5 2))
            )
        FetchRandom (randNum, randWait) ->
            ( { model | nameCounter = nameCounter + 1 }
            , fetchData randNum randWait nameCounter
                |> Task.perform FetchData
            )
        FetchData newName ->
            { model | nameList = newName :: nameList } ! []
        Reset ->
            { model | nameList = [] } ! []

view : Model -> Html Msg
view model =
    let
        phrase =
            case model.nameList of
                [] -> ""
                [n1] -> "one word: " ++ n1
                [n1,n2] -> "two words: " ++ n1 ++ ", " ++ n2
                [n1, n2, n3] ->
                    "The " ++ n1
                    ++ " and " ++ n2 ++ " catches the "
                    ++ n3 ++ " by the tail"
                _ ->
                    "Too many words! reset!"
        resetButton =
            case model.nameList of
                _::_::_::_ ->
                    [ button [onClick Reset] [ text "reset!"] ]
                _ -> []

    in
        div []
            (
                [ button [onClick GetRandom] [ text "generate a name!" ]
                , span [] [text phrase]
                ] ++ resetButton
            )
