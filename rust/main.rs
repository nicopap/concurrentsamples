use std::thread;
use std::time::Duration;
use std::sync::{Arc,Mutex};
use std::sync::mpsc::{Sender,Receiver,channel};

fn incr(sum : Arc<Mutex<i32>>, send : Sender<i32>) {
    const multiple: i32 = 10;
    loop {
        { //la lock est associée a ce scope, meurt à la fin
            let mut sum_ = sum.lock().unwrap();
            let tmp = *sum_ + 1;
            *sum_ = tmp;
            if tmp % multiple == 0 {
                send.send(tmp).unwrap();
            }
        } // si la ligne suivante était dans
        // le scope de la lock, un thread
        // maintient le mutex pour une seconde
        // donc seulement un thread peut agir
        // par seconde.
        thread::sleep(Duration::from_secs(1));
    }
}

fn collect_sum(receive : Receiver<i32>) {
    let mut i = 1;
    loop {
        let val = receive.recv().unwrap();
        print!("{}: {}\n", i, val);
        i += 1;
    }
}

fn main() {
    let sum = Arc::new(Mutex::new(0));
    let (send, receive) = channel();
    for _ in 0..10 {
        let (sum_, send_) = (sum.clone(), send.clone());
        thread::spawn(move || {incr(sum_, send_);});
    }
    let join_handle = thread::spawn(move || {collect_sum(receive);});
    join_handle.join(); //attend indéfiniment sur collect_sum
}
