with Ada.Numerics.Discrete_Random;
with Ada.Text_IO;

procedure Guess_Number is
	task Process_Guess is
		entry Process (Value : in Integer);
	end Process_Guess;
	task body Process_Guess is
		subtype Number is Integer range 1 .. 10;
		package Number_RNG is new Ada.Numerics.Discrete_Random (Number);
		package Number_IO is new Ada.Text_IO.Integer_IO (Number);
		Generator : Number_RNG.Generator;
		Answer : Number;
		Guess : Number;
	begin
		Number_RNG.Reset (Generator);
		Answer := Number_RNG.Random(Generator);
		loop
			accept Process (Value : in Integer) do
				Guess := Number(Value mod 10);
			end Process;
			if Guess = Answer then
				Number_IO.Put( Guess );
				Ada.Text_IO.Put(" is the correct answer");
				exit;
			else
				Number_IO.Put( Guess );
				Ada.Text_IO.Put(" is wrong!");
			end if;
		end loop;
		Ada.Text_IO.Put_Line ("Congratulation!");
	end Process_Guess;

	task Collect_Guess is
		entry Collect (Value : out Integer);
	end Collect_Guess;
	task body Collect_Guess is
		package Integer_IO is new Ada.Text_IO.Integer_IO (Integer);
		Guess : Integer;
	begin
		loop
			Ada.Text_IO.Put("guess a number: ");
			Integer_IO.Get (Guess);
			accept Collect (Value : out Integer) do
				Value := Guess;
			end Collect;
		end loop;
	end Collect_Guess;
	Guess : Integer;
begin
	loop
		Collect_Guess.Collect(Guess);
		Ada.Text_IO.New_Line;
		Process_Guess.Process(Guess);
	end loop;
end Guess_Number;

